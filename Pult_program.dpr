program Pult_program;

uses
  Forms,
  panel in 'panel.pas' {Form1},
  coarse in 'coarse.pas' {CoarseF},
  delet in 'delet.pas' {DeleteF},
  Watch in 'Watch.pas' {WatchF},
  pairs in 'pairs.pas' {PairF},
  Main_prog in 'Main_prog.pas' {MainsF},
  prehistory in 'prehistory.pas' {HistF},
  GMTime in 'GMTime.pas' {TimeF},
  Unit1 in 'Unit1.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title:='PNL';
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TMainsF, MainsF);
  Application.CreateForm(TDeleteF, DeleteF);
  Application.CreateForm(TWatchF, WatchF);
  Application.CreateForm(TPairF, PairF);
  Application.CreateForm(THistF, HistF);
  Application.CreateForm(TCoarseF, CoarseF);
  Application.CreateForm(TTimeF, TimeF);
  Application.Run;
end.
