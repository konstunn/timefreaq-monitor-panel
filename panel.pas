unit panel;
{$I-}
//�������  ����
 interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Menus, ExtCtrls, TeeProcs, TeEngine, Chart, Series, Math,FileCtrl,
   DbChart, TeeFunci, Spin, ComCtrls, shellapi,Grids,inifiles;

type
  TBaudRate = (cbr110, cbr300, cbr600, cbr1200, cbr2400, cbr4800, cbr9600,
    cbr14400, cbr19200, cbr38400, cbr56000, cbr57600, cbr115200, cbr128000, cbr256000);
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    EXITPROG1: TMenuItem;
    Presets1: TMenuItem;
    Label1: TLabel;
    Scalecoarse1: TMenuItem;
    WatchthePair1: TMenuItem;
    DeleteScale1: TMenuItem;
    SaveChanges1: TMenuItem;
    RunProgram1: TMenuItem;
    MatrixCorrection1: TMenuItem;
    Ordinary1: TMenuItem;
    RunNow1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    SaveDialog1: TSaveDialog;
    Panel1: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    PairEdit1: TEdit;
    N7: TMenuItem;
    Receiver: TComboBox;
    N5: TMenuItem;
    N3641: TMenuItem;
    N36411: TMenuItem;
    kont1: TCheckBox;
    procedure TimeConv(Sender: TObject);
    procedure EXITPROG1Click(Sender: TObject);
    procedure SaveChanges1Click(Sender: TObject);
    procedure Scalecoarse1Click(Sender: TObject);
    procedure DeleteScale1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WatchthePair1Click(Sender: TObject);
    procedure MatrixCorrection1Click(Sender: TObject);
    procedure Ordinary1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure RunNow1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N3641Click(Sender: TObject);
    procedure N36411Click(Sender: TObject);
    procedure ReadINIFile;
    procedure SaveINIFile;
    procedure FormCreate(Sender: TObject);
     private
    { Private declarations }
  public
    { Public declarations }
      hPort: THandle;
  end;

  Ttabl = array[0..16,0..16] of byte;
  TSets = array[0..18] of longint;
  Temerg= array[1..18,1..18,1..144] of Real;
  Tsh   = array[1..18,1..18,1..4]   of Real;//������ ���������� ��������
  Tdfb = array[0..16] of byte;//��������� ����������
  TBuff_P= array[0..25,0..25,1..2] of Double;
var
 portcomm,portmeter:string;
 ninifile: tinifile;
 ffirstrun:byte;
 contr_str,str_ichas:String;
 ss:array[1..8] of byte;
 mass:string;
 count,f_open,timmeter,freadcomm:byte;
 flag_power_off:byte;//���� ���������� ��������� .
 Form1: TForm1;
 rcv:byte;

 f_init: file of Ttabl;
 f_var:shortstring;

 f_Sets: file of TSets;
 f_set:shortstring;

 f_alarm:file of Temerg;
 f_sh:   file of Tsh;

 f_dels:file of Tdfb;
 f_ddel:shortstring;


 df,m_data,s_data:real;
 time_r,time_f:ShortString;
 f_scale:shortstring;
 f_alr:shortstring;
 main_dir,tstmom:ShortString;
 testmom,f_dat,f_datt,f_hrs,f_tmp:Text;
 f_dvar,ful_name:shortstring;
 f_show,r1,r2,k1,k2,y1,y2:byte;
 p1,p2:shortstring;
 i_st,res1,dopm,idata:integer;
 c1,c2,dw1,dw2,pg1,pg2:integer;
 gmt,mmt,lmt,ms,st1,st2:shortstring;
 lh,mh,gh,i,j:integer;
 chas:real;
 kont,ichas:Real;
 err_buf,kolizm,period,nizm:byte;
 hour,min,sec,msec,TimeSwitch:word;
 year,month,day:word;
 f_P:File of TBuff_P;//
 Buff_P:TBuff_P;
 tabl:Ttabl;
 sets:TSets;
 emerg:Temerg;
 dfb:Tdfb;
 dc,tc:longint;
 sh:Tsh;
 f_zero:byte;//���� 1-�� ���������
 f_imm:byte;//���� ������������ �������
 f_ex: byte;//���� �������� ����� �������
 fname:string;//��� �������� ����� ������
  tsw:array[1..6]   of byte=(05,15,25,35,45,55);//����� ������ �� ���������
  cal:array[1..12]  of byte=(31,28,31,30,31,30,31,31,30,31,30,31);//���������
  b:  array[1..22]  of byte;//������ � �3-64
 post:array[1..256] of Double; //��� ���������� �������� .
 tmp :array[0..16,0..16] of byte;
 stat:array[1..8192] of real;
implementation

uses coarse, Watch, delet, GMTime, pairs, Main_prog;
{$R *.DFM}

procedure Tform1.TimeConv;
//Converts system time to its
//Greenwich,Moscow and Local values.
begin
 lmt:=TimeToStr(time);
  ms:=lmt;
   if length(lmt)=7 then delete(lmt,2,6)
   else
     delete(lmt,3,6);
     gh:=StrToInt(lmt);
     mh:=gh+3;
     if mh >=24 then mh:=mh-24;
     lh:=gh+6;
     if lh >=24 then lh:=lh-24;
   if length(ms)=7 then delete(ms,1,1)
    else
     delete(ms,1,2);
    gmt:=IntToStr(gh)+ms;
    mmt:=IntToStr(mh)+ms;
    lmt:=IntToStr(lh)+ms;
end;


procedure TForm1.EXITPROG1Click(Sender: TObject);
begin
 close;
end;



procedure TForm1.SaveChanges1Click(Sender: TObject);
begin
saveINIFile;
f_var:='c:/exes/room/matrix.ini';//�������
res1:=0;
assignfile(f_init,f_var);
rewrite(f_init);
write(f_init,tabl);
closefile(f_init);

f_ddel:='c:/exes/room/deleted.ini';//������� �������� �����������
assignfile(f_dels,f_ddel);
rewrite(f_dels);
write(f_dels,dfb);
closefile(f_dels);

f_scale:='c:/exes/room/scale.ini';//�������
assignfile(f_sh,f_scale);
rewrite(f_sh);
write(f_sh,sh);
closefile(f_sh);

f_alr:='c:/exes/room/prehistory.sav';//�����������
assignfile(f_alarm,f_alr);
rewrite(f_alarm);
write(f_alarm,emerg);
closefile(f_alarm);
end;

procedure TForm1.Scalecoarse1Click(Sender: TObject);//�������
begin
 CoarseF.CoarseME.Text:=IntToStr(dopm);
 CoarseF.ShowModal;
end;

procedure TForm1.WatchthePair1Click(Sender: TObject);//���.����
var
s:string;
begin
 WatchF.ShowModal;
end;

procedure TForm1.DeleteScale1Click(Sender: TObject);//����� �������� ����
var
 s: string;
begin
 s:='';
// if d1<10 then s:='0'+IntToStr(d1)+' ' else s:=IntToStr(d1)+' ';
// if d2<10 then s:=s+'0'+IntToStr(d2)+' ' else s:=s+IntToStr(d2)+' ';
// if d3<10 then s:=s+'0'+IntToStr(d3) else s:=s+IntToStr(d3);
 DeleteF.ShowModal;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
 GetDir(0,main_dir);
 setcurrentdir('main_dir');
if not( DirectoryExists('c:\exes\')) then Createdir('c:\exes\');
if not( DirectoryExists('c:\exes\room\')) then Createdir('c:\exes\room\');

ffirstrun:=1;
readINIFile;

 DecodeTime(TdateTime(time),hour,min,sec,msec);
 DecodeDate(TdateTime(date), Year, Month, Day);

    twatch:=TimeToStr(time);
    assignfile(f_tmp,'c:/exes/'+'logfile.txt');
    if fileexists('c:/exes/'+'logfile.txt')  then append(f_tmp) else  rewrite(f_tmp);
    writeln(f_tmp,' Date :  '+IntToStr(day)+'/'+IntToStr(month)+'/'+IntToStr(year)+'   Time :  '+twatch+'(  ������ ��������� )');
    closefile(f_tmp);//logfile

f_scale:='c:/exes/room/scale.ini';//�������
assignfile(f_sh,f_scale);
reset(f_sh);
read(f_sh,sh);
closefile(f_sh);

f_alr:='c:/exes/room/prehistory.sav';//�����������
assignfile(f_alarm,f_alr);
reset(f_alarm);
read(f_alarm,emerg);
closefile(f_alarm);

f_var:='c:/exes/room/matrix.ini';//�������
res1:=0;
assignfile(f_init,f_var);
reset(f_init);
read(f_init,tabl);
closefile(f_init);

f_ddel:='c:/exes/room/deleted.ini';//������� �������� �����������
assignfile(f_dels,f_ddel);
reset(f_dels);
read(f_dels,dfb);
closefile(f_dels);

end;




procedure TForm1.MatrixCorrection1Click(Sender: TObject);//����� ��� � ��������� ������� ���������
begin
PairF.ShowModal;
end;

procedure TForm1.Ordinary1Click(Sender: TObject);
begin
MainsF.Timer1.Enabled:=True;
MainsF.Showmodal;
end;

procedure TForm1.N1Click(Sender: TObject);
begin
f_zero:=1;
idata:=0;
nizm:=1;
MainsF.Showmodal;
end;

procedure TForm1.RunNow1Click(Sender: TObject);
begin
 f_imm:=1;
 MainsF.Timer1.Enabled:=True;
 MainsF.Showmodal;
end;

procedure TForm1.N2Click(Sender: TObject);
begin
FormShow(Sender);
end;

procedure TForm1.N3Click(Sender: TObject);
begin
//������ ����� � ������ �������������
saveINIFile;

    twatch:=TimeToStr(time);
    assignfile(f_tmp,'c:/exes/'+'logfile.txt');
    if fileexists('c:/exes/'+'logfile.txt')  then append(f_tmp) else  rewrite(f_tmp);
    writeln(f_tmp,' Date :  '+IntToStr(day)+'/'+IntToStr(month)+'/'+IntToStr(year)+'   Time :  '+twatch+'  (������� ��������� )');
    closefile(f_tmp);//logfile


f_var:='c:/exes/room/matrix.ini';//�������
res1:=0;
assignfile(f_init,f_var);
rewrite(f_init);
write(f_init,tabl);
closefile(f_init);

f_ddel:='c:/exes/room/deleted.ini';//������� �������� �����������
assignfile(f_dels,f_ddel);
rewrite(f_dels);
write(f_dels,dfb);
closefile(f_dels);

f_scale:='c:/exes/room/scale.ini';//�������
assignfile(f_sh,f_scale);
rewrite(f_sh);
write(f_sh,sh);
closefile(f_sh);

f_alr:='c:/exes/room/prehistory.sav';//�����������
assignfile(f_alarm,f_alr);
rewrite(f_alarm);
write(f_alarm,emerg);
closefile(f_alarm);
close;
end;


procedure TForm1.N5Click(Sender: TObject);
begin
  TimeF.ShowModal;
  close;
end;


procedure TForm1.N3641Click(Sender: TObject);//�3-64
begin
timmeter:=0;
end;

procedure TForm1.N36411Click(Sender: TObject);//�3-64.1
begin
timmeter:=1;
end;


procedure TForm1.ReadINIFile;
begin
 ninifile := TIniFile.Create('c:/exes/Room/tuneall.ini');
 dopm:=ninifile.readinteger('�������','������ �� �����',dopm);
 dw1:=ninifile.readinteger('Const','Const dw1',dw1);
 dw2:=ninifile.readinteger('Const','Const dw2',dw2);
 nizm:=ninifile.readinteger('�����','N �������� ���������',nizm);
 fname:=ninifile.readstring('�����','��� �����',fname);
 idata:=ninifile.readinteger('����','������� ����',idata);
 f_ex:=ninifile.readinteger('Const','Const f_ex',f_ex);
 TimeSwitch:=ninifile.readinteger('����','����� ����� ���������',TimeSwitch);
 portcomm:=ninifile.readstring('�����','���� �����������', portcomm);
 portmeter:=ninifile.readstring('�����','���� �3-64', portmeter);
ninifile.free;
end;

procedure  TForm1.SaveINIFile;
begin
 ninifile := TIniFile.Create('c:/exes/Room/tuneall.ini');
 ninifile.writeinteger('�������','������ �� �����',dopm);
 ninifile.writeinteger('Const','Const dw1',dw1);
 ninifile.writeinteger('Const','Const dw2',dw2);
 ninifile.writeinteger('�����','N �������� ���������',nizm);
 ninifile.writestring('�����','��� �����',fname);
 ninifile.writeinteger('����','������� ����',idata);
 ninifile.writeinteger('Const','Const f_ex',f_ex);
 ninifile.writeinteger('����','����� ����� ���������',TimeSwitch);
 ninifile.writeinteger('������','�3-64 ��� �3-64\1', Timmeter);
 ninifile.writestring('�����','���� �����������', portcomm);
 ninifile.writestring('�����','���� �3-64', portmeter);
 ninifile.free;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  hMutex : THandle;
begin
  hMutex := CreateMutex(0, true , 'pult_program.exe');
  if GetLastError = ERROR_ALREADY_EXISTS then
  begin
  showmessage('��� ��������� ��� �������� !');
    CloseHandle(hMutex);
    Application.Terminate;
  end;
end;


end.
