unit prehistory;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ComCtrls, Spin, Dialogs, Grids;

type
  THistF = class(TForm)
    Button1: TButton;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    hist1: TStringGrid;
    hist2: TStringGrid;
    procedure Button1Click(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpinEdit2Change(Sender: TObject);
    procedure Change1;
    procedure Change2;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  HistF: THistF;
m:byte;
t_izm:LongInt;
implementation
uses panel;
{$R *.DFM}
var
fr,sh:byte;

procedure THistF.FormShow(Sender: TObject);
begin
HistF.Caption:='Prehistory ';
SpinEdit1.Value:=nizm;
SpinEdit2.Value:=nizm;
change1;
change2;
end;


procedure THistF.Button1Click(Sender: TObject);
begin
close;
end;

procedure THistF.SpinEdit1Change(Sender: TObject);
begin
change1;
end;


procedure THistF.SpinEdit2Change(Sender: TObject);
begin
change2;
end;


procedure THistF.Change1;
var
m,c1,c2:byte;
y,n:Integer;
txt,tims1,dats1,dt:string;
dc,d,mon,th,h,m1,secs:longint;
s1,r1:LongInt;
r,s,ttt:Double;
begin
//����� ������
              if SpinEdit1.Value=145 then SpinEdit1.Value:=1;
              if SpinEdit1.Value=0 then SpinEdit1.Value:=144;
              m:=SpinEdit1.Value;
              min:=1;
              th:=Trunc(emerg[18,1,m]);//����� ������ �� ������� ��������� .
              h:=th div 10000;
              th:=th-h*10000;
              m1:=th div 100;
              secs:=th-m1*100;
              tims1:='  '+IntToStr(h)+':'+IntToStr(m1)+':'+IntToStr(secs);

              dc:=Trunc(emerg[18,2,m]);
              d:=dc div 100;
              mon:=(dc-d*100);

              dats1:='  '+IntToStr(d)+'-'+IntToStr(mon);
              with hist1 do cells[0,0]:='           ';
              with hist1 do cells[1,0]:='   �����   ';
              with hist1 do cells[2,0]:='   ����    ';
              with hist1 do cells[3,0]:='           ';

              with hist1 do cells[1,1]:=tims1;
              with hist1 do cells[2,1]:=dats1;
//-----------------------------------------------------
              with hist1 do cells[0,2]:='   ����    ';
              with hist1 do cells[1,2]:='  ������   ';
              with hist1 do cells[2,2]:=' dt(10���) ';
              with hist1 do cells[3,2]:='....����...';

n:=2;
              for c1:=1 to 16 do
              for c2:=1 to 16 do
              if (tmp[c1,c2]>0) then
                   begin
                    n:=n+1;
                    p1:=IntToStr(c1);
                    p2:=IntToStr(c2);
            if c1>9 then p1:=chr(c1+55);
            if c2>9 then p2:=chr(c2+55);
                    txt:=' s'+p2+'-s'+p1;
                    with hist1 do cells[0,n]:=txt;
                   end;



n:=2;
              for c1:=1 to 16 do
              for c2:=1 to 16 do
              if (tmp[c1,c2]>0) then
                begin
                    n:=n+1;
                    p1:=IntToStr(c1);
                    p2:=IntToStr(c2);
                    s:=(emerg[c1,c2,m]);
                    r:=s-(emerg[c1,c2,m-1]);
                    if c1>9 then p1:=chr(c1+55);
                    if c2>9 then p2:=chr(c2+55);
                if (abs(r)>999999500)and(r>0)then r:=-(1e9-r);
                if (abs(r)>999999500)and(r<0)then r:=1e9-abs(r);
                dt:=FloatToStrF(r,ffFixed,10,1);
                if abs(r)>dopm then dt:=dt+'  *';
                  hist1.Cells[1,n]:=FloatToStrF(s,ffFixed,10,1);
                  hist1.Cells[2,n]:=FloatToStrF(r,ffFixed,10,1);
                  hist1.Repaint;
                end;
end;

procedure THistF.Change2;
var
m,c1,c2:byte;
y,n:Integer;
txt,tims1,dats1,dt:string;
dc,d,mon,th,h,m1,secs:longint;
s1,r1:LongInt;
r,s,ttt:Double;
begin
//����� ������
              if SpinEdit2.Value=145 then SpinEdit1.Value:=1;
              if SpinEdit2.Value=0 then SpinEdit1.Value:=144;
              m:=SpinEdit2.Value;
              min:=1;
              th:=Trunc(emerg[18,1,m]);//����� ������ �� ������� ��������� .
              h:=th div 10000;
              th:=th-h*10000;
              m1:=th div 100;
              secs:=th-m1*100;
              tims1:='  '+IntToStr(h)+':'+IntToStr(m1)+':'+IntToStr(secs);

              dc:=Trunc(emerg[18,2,m]);
              d:=dc div 100;
              mon:=(dc-d*100);

              dats1:='  '+IntToStr(d)+'-'+IntToStr(mon);
              with hist2 do cells[0,0]:='           ';
              with hist2 do cells[1,0]:='   �����   ';
              with hist2 do cells[2,0]:='   ����    ';
              with hist2 do cells[3,0]:='           ';

              with hist2 do cells[1,1]:=tims1;
              with hist2 do cells[2,1]:=dats1;
//-----------------------------------------------------
              with hist2 do cells[0,2]:='   ����    ';
              with hist2 do cells[1,2]:='  ������   ';
              with hist2 do cells[2,2]:=' dt(10���) ';
              with hist2 do cells[3,2]:='....����...';

n:=2;
              for c1:=1 to 16 do
              for c2:=1 to 16 do
              if (tmp[c1,c2]>0) then
                   begin
                    n:=n+1;
                    p1:=IntToStr(c1);
                    p2:=IntToStr(c2);
            if c1>9 then p1:=chr(c1+55);
            if c2>9 then p2:=chr(c2+55);
                    txt:=' s'+p2+'-s'+p1;
                    with hist2 do cells[0,n]:=txt;
                   end;



n:=2;
              for c1:=1 to 16 do
              for c2:=1 to 16 do
              if (tmp[c1,c2]>0) then
                begin
                    n:=n+1;
                    p1:=IntToStr(c1);
                    p2:=IntToStr(c2);
                    s:=(emerg[c1,c2,m]);
                    r:=s-(emerg[c1,c2,m-1]);
                    if c1>9 then p1:=chr(c1+55);
                    if c2>9 then p2:=chr(c2+55);
                if (abs(r)>999999500)and(r>0)then r:=-(1e9-r);
                if (abs(r)>999999500)and(r<0)then r:=1e9-abs(r);
                dt:=FloatToStrF(r,ffFixed,10,1);
                if abs(r)>dopm then dt:=dt+'  *';
                  hist2.Cells[1,n]:=FloatToStrF(s,ffFixed,10,1);
                  hist2.Cells[2,n]:=FloatToStrF(r,ffFixed,10,1);
                  hist2.Repaint;
                end;
end;


end.

