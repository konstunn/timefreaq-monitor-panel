unit Main_prog;
//������� ���������
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Menus, ExtCtrls, TeeProcs, TeEngine, Chart, Series, Math,FileCtrl,
   DbChart, TeeFunci, Spin, ComCtrls, shellapi,Grids,inifiles;
type
  TMainsF = class(TForm)
    Button1: TButton;
    Timer1: TTimer;
    Label1: TLabel;
    Memo4: TMemo;
    Label2: TLabel;
    Label3: TLabel;
    Button2: TButton;
    Button3: TButton;
    Edit1: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    moments: TStringGrid;
    chchan: TStringGrid;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Mainwork;
    procedure Timer1Timer(Sender: TObject);
    procedure ReadText;
    procedure izmer;
    procedure Execom;
    procedure OpenPort;
    procedure tint64;
  private
    procedure TimeConv;
    { Private declarations }
  public
hPort,sPort: THandle;
icom_str:String;
ds:array[1..30] of byte;
str1:string;
DCB: TDCB;
CommTimeOuts:TCommTimeOuts;
m64:array[1..40] of char;
  comportname:String;
  running:byte;
  chas,kont:real;
  bytesread,tread:dword;
    { Public declarations }
  end;
var
  MainsF: TMainsF;
  Present:TDateTime;
implementation

uses panel, prehistory,Watch;
{$R *.DFM}

procedure TMainsF.TimeConv;
//Converts system time to its
//Greenwich,Moscow and Local values.
var
mq,sq:shortstring;
begin
DecodeTime(TdateTime(time),hour,min,sec,msec);
 if min <9 then mq:='0'+IntToStr(min) else mq:=IntToStr(min);
 if sec <9 then sq:='0'+IntToStr(sec) else sq:=IntToStr(sec);
 ms:=':'+mq+':'+sq;
     gh:=hour;
     mh:=gh+3;
     if mh >=24 then mh:=mh-24;
     lh:=gh+6;
     if lh >=24 then lh:=lh-24;
     if gh<10 then gmt:='0'+IntToStr(gh)+ms else gmt:=IntToStr(gh)+ms;
     if mh<10 then mmt:='0'+IntToStr(mh)+ms else mmt:=IntToStr(mh)+ms;
     if lh<10 then lmt:='0'+IntToStr(lh)+ms else lmt:=IntToStr(lh)+ms;
end;


procedure TMainsF.FormShow(Sender: TObject);
var
x,y,c1,c2:integer;
txt:string;
begin
 Timer1.Enabled:=True;
 Label1.Visible:=True;
 Edit1.Visible:=True;
 rcv:=0;
Application.ProcessMessages;
if length(f_dvar)=3 then f_dvar:='0'+f_dvar;
 for c1:=0 to 16 do
   for c2:=0 to 16 do
      begin
        tmp[c1,c2]:=tabl[c1,c2];
        if((dfb[c1]=0)or(dfb[c2]=0)) then tmp[c1,c2]:=0;
      end;
              with Moments do cells[0,0]:='    �����  ';
              with Moments do cells[1,0]:='   ��������';
              with Moments do cells[2,0]:='       ���   ';
              with Moments do cells[3,0]:='....����....';
              y:=1;
              for x:=1 to 30 do Moments.Cells[0,x]:=' ';

              for c1:=0 to 16 do
              for c2:=0 to 16 do
              if (tmp[c1,c2]>=1) then
                   begin
                    p1:=IntToStr(c1);
                    p2:=IntToStr(c2);
            if c1>9 then p1:=chr(c1+55);
            if c2>9 then p2:=chr(c2+55);
                    txt:=' s'+p2+'-s'+p1;
                    with Moments do cells[0,y]:=txt;
                    y:=y+1;
                   end;
//
   ChChan.Cells[0,0]:='���.';
   ChChan.Cells[0,1]:='1S';
with ChChan do
   for x:=1 to 16 do
   begin
   cells[x,0]:=IntToHex(x-1,2);
   end;
   Label6.Visible:=True;
   Label6.Caption:='    ������� 1-sec �������� �� ������ �����������.';
end;

procedure TMainsF.Button1Click(Sender: TObject);
//����� � ������� ����.
begin
Timer1.Enabled:=False;
close;
end;

procedure TMainsF.Timer1Timer(Sender: TObject);
var
i:longint;
begin
 DecodeTime(TdateTime(time),hour,min,sec,msec);
 DecodeDate(TdateTime(date), Year, Month, Day);
 Timeconv;
 Label1.Caption:='UTC = '+gmt;
 Label4.Caption:='MSK = '+mmt;
 Label5.Caption:='NSK = '+lmt;
 dc:=day*100+month;
 tc:=hour*10000+min*100+sec;
 if (sec-(sec div 10)*10=5) then
        begin
           WatchF.readfromcomm;//������ �������������� ������������ ������� ���
           ReadText;           //������ ��������� ������ �����������
        end;

 for i:=1 to 6 do if(( min=tsw[i])and(sec<=3))or(f_imm=1) then
         begin
                button1.Enabled:=False;
                button2.Enabled:=False;
                button3.Enabled:=False;
                 Mainwork;
                button1.Enabled:=True;
                button2.Enabled:=True;
                button3.Enabled:=True;
         end;
end;

procedure TMainsF.Mainwork;
label m1,no_data;
var
x,y,f1,i1,i,c1,c2,pr1,pr2,dpr1,dpr2,ind1,ind2:byte;
txt:shortstring;
a,s,fp,result:double;
t1,t2,t3:longint;
scale:char;
pm1,pm2:int64;
n,j,m:integer;
f_err,t:string;
num_p:integer;
v,gs,r1,r2,k1,k2,k:byte;
ferr:TextFile;
tval,gd:string;
test:array[1..8] of Double;
begin
//������� ���� ������ �� ���������
    if f_zero=1 then nizm:=1;
    if (hour=TimeSwitch)and(min=10)and(sec<=3) then nizm:=1;
    begin
    if nizm=1 then
          begin
               f_dvar:=chr((day div 10)+48)+chr((day-10*(day div 10))+48);
               f_dvar:=f_dvar+chr((month div 10)+48)+chr((month-10*(month div 10))+48);
               gd:=IntToStr(year);
               delete(gd,1,2);
               f_dvar:=f_dvar+gd;
               ful_name:='c:/exes/'+f_dvar;
               assignfile(f_dat,ful_name+'.dat');
               assignfile(f_hrs,ful_name+'.hrs');
               fname:=(f_dvar);
               rewrite(f_dat);
               closefile(f_dat);
               rewrite(f_hrs);
               closefile(f_hrs);
          end;

               f_dvar:=(fname);
               if length(f_dvar)=5 then f_dvar:='0'+f_dvar;
               ful_name:='c:/exes/'+f_dvar;
               assignfile(f_dat,ful_name+'.dat');
               assignfile(f_hrs,ful_name+'.hrs');

               if fileexists(ful_name+'.dat') then append(f_dat) else  rewrite(f_dat);
               if fileexists(ful_name+'.hrs') then append(f_hrs) else  rewrite(f_hrs);


               assignfile(f_tmp,'c:/exes/'+'logfile.txt');
               if(day=13)and(hour=13) then rewrite(f_tmp);//����� ������ 13 ����� ������� ������
               if fileexists('c:/exes/'+'logfile.txt')  then append(f_tmp) else  rewrite(f_tmp);

begin
         txt:='Pass = '+IntToStr(nizm);
         tval:=TimeToStr(time);
         Edit1.Clear;
         Edit1.SelText:=txt;
         num_p:=0;
         f_imm:=0;
 if nizm=1 then//������ ����� ������
          begin
              for c1:=1 to 16 do
              for c2:=1 to 16 do
              if tmp[c1,c2]>=1 then
                begin
                    p1:=IntToStr(c1);
                    p2:=IntToStr(c2);
                    if c1>9 then p1:=chr(c1+55);
                    if c2>9 then p2:=chr(c2+55);
                    if (tmp[c1,c2]=1)or(tmp[c1,c2]=2)or(tmp[c1,c2]=3) then
                        write(f_dat,p2,' - ',p1,'         '); //����� ��� f_dat
                    if tmp[c1,c2]=2 then
                        write(f_hrs,p2,' - ',p1,'         '); //����� ��� f_hrs
                end;
                    writeln(f_dat);
                    writeln(f_hrs);
          end;
         j:=0;
Application.ProcessMessages;
Moments.Canvas.Brush.Color :=clCaptionText;
Moments.Focused;
Moments.Repaint;
         for c1:=1 to 16 do
         for c2:=1 to 16 do
         if (tmp[c1,c2]>=1)and(tmp[c1,c2]<=4) then
begin//   ���� - ���������
            p1:=IntToStr(c1);
            p2:=IntToStr(c2);
            if c1>9 then p1:=chr(c1+55);
            if c2>9 then p2:=chr(c2+55);
            num_p:=num_p+1;//���������� ����� ���������� ����
            //
            //
            if(p2='A')and(p1='B') then t1:=n;
            if(p2='A')and(p1='C') then t2:=n;
            if(p2='B')and(p1='C') then t3:=n;
            s:=0;
            dw2:=c1;
            dw1:=c2;
            ss[4]:=$52;
            WatchF.sendtocomm;//����������
            ffirstrun:=1;//���������� �3-64 ��� ����� ����.
            sleep(500);
                 emerg[18,1,nizm]:=tc;//������ ���������
                 emerg[18,2,nizm]:=dc;//���� ���������
                 for i:=1 to 2 do
                 begin
                      if timmeter=0 then izmer;//�3-64
                      if timmeter=1 then execom;//�3-64/1
                 end;
               for m:=1 to 5 do test[m]:=0;
               txt:='s'+p2+'-s'+p1;
               k:=0;
               v:=0;
               s:=0;
               ind1:=0;
               ind2:=0;
               for m:=1 to 5 do
                  begin
                     if timmeter=0 then izmer;
                     if timmeter=1 then execom;
                     test[m]:=ichas;
                     Moments.Cells[1,num_p]:=FloatToStrF(ichas,ffFixed,10,1);
                     Moments.Cells[2,num_p]:='   N = '+IntToStr(m);
                     Moments.Cells[3,num_p]:='';
                     Moments.Repaint;
                  end;
                  for i:=1 to 4 do if abs(test[i+1]-test[i])<=dopm then
                                begin
                                if i=1 then s:=s+test[i]+test[i+1] else s:=s+test[i+1];
                                if i=1 then k:=k+2 else k:=k+1;
                                end;
                                s:= s/k;
m1:
             if k>0 then
             begin
                result:=s-sh[c1,c2,1];  //��� i-� ����
                if (abs(result)>999999900)and(result>0)then result:=-(1e9-result);
                if (abs(result)>999999900)and(result<0)then result:=1e9-abs(result);
                sh[c1,c2,1]:=s;
                if abs(result) <= dopm then sh[c1,c2,2]:=s;//����������� �������� �� ������ ���� .
                emerg[c1,c2,nizm]:=s;//������ �������� ��������� � �����������
                if min=15 then
                        begin
                          buff_P[c2,c1,1]:=buff_P[c2,c1,2];
                          buff_P[c2,c1,2]:=s;
                        end;
             end;
             if( k=0)then
                begin
                 emerg[c1,c2,nizm]:=sh[c1,c2,1];//���� ��� ���.-�� �������� ������.���.
                 if (tmp[c1,c2]=1)or(tmp[c1,c2]=2)or(tmp[c1,c2]=3) then
                    write(f_dat,'--��� ���--','   ');
                    writeln(f_tmp,'n= '+IntToStr(nizm)+'  '+txt+'--��� ���--');//LogFile
                 if ((nizm>=1)and(min>=55)and(min<=59)) then
                 if tmp[c1,c2]=2 then write(f_hrs,'--��� ���--','   ');
                 if (tmp[c1,c2]>=2)and(tmp[c1,c2]<=4) then
                    Moments.Cells[3,num_p]:=' ��� ��������� !';
                    Moments.Repaint;
                 goto no_data;

                end;
//����� � 1-� ��������� .
            if ((nizm=1)and(tmp[c1,c2]>=2)and(tmp[c1,c2]<=4))
             then Moments.Cells[1,num_p]:=FloatToStrF(s,ffFixed,10,1);
                  Moments.Repaint;
//����� � ��������� ��������� .
            if ((nizm>1)and(tmp[c1,c2]>=2)and (tmp[c1,c2]<=4))
             then
                begin
                  Moments.Cells[1,num_p]:=FloatToStrF(s,ffFixed,10,1);
                  Moments.Cells[2,num_p]:=FloatToStrF(result,ffFixed,10,1);
                  if abs(result)>=dopm then
                        begin
                          Moments.Cells[3,num_p]:='  ���� ! ';
                          Memo4.Lines.Add(txt+' ���� ! dt = '+FloatToStrF(result,ffFixed,10,1)+' '+gmt);
                          writeln(f_tmp,'n= '+IntToStr(nizm)+'  '+txt+' ���� ! dt = '+FloatToStrF(result,ffFixed,10,1)+' '+gmt);//LogFile
                        end;
                end;
                  Moments.Repaint;
            if (tmp[c1,c2]=1)or(tmp[c1,c2]=2)or(tmp[c1,c2]=3) then write(f_dat,s:10:1,'   ');
// 1-��� ������ � ���� f_dat.
// 2-��� ������ � ���� f_hrs.
// 3-��� ������ �� �����.
// 4-��� �������.
// 5-��������� ����.
   if ((nizm>=1)and(min>=55)and(min<=59)) then if tmp[c1,c2]=2 then write(f_hrs,s:10:1,'   ');
no_data:
            j:=j+1;
end;//   ���� - ���������
//����� n-�� ���������
        writeln(f_dat,tval,'  ',nizm);
        if ((nizm>=1)and(min>=55)and(min<=59)) then writeln(f_hrs,'  ',tval);
        t1:=Trunc(emerg[11,10,nizm]);//AB
        if (t1>999999900) then t1:=-(1000000000-t1);
        t2:=Trunc(emerg[12,10,nizm]);//AC
        if (t2>999999900) then t2:=-(1000000000-t2);
        t3:=Trunc(emerg[12,11,nizm]);//BC
        if (t3>999999900) then t3:=-(1000000000-t3);
        t1:=abs(t1-t2);//AB-AC
        t2:=abs(t1-t3);//(AB-AC)-BC
        if t2>10 then t2:=abs(t1+t3);
        Moments.Cells[0,num_p+1]:=' ������(ABC) = ';
        Moments.Cells[1,num_p+1]:='   '+IntToStr(t2)+'  ��';
        Moments.Repaint;
        writeln(f_tmp,'n= '+IntToStr(nizm)+'   ������(ABC) = '+IntToStr(t2)+'  ��');//LogFile
        nizm:=nizm+1;
        if nizm=145 then
        begin
        nizm:=1;//����� ���������
        writeln(f_tmp,'-----------------------------------------------');
        writeln(f_tmp,'  ����� ����� Date :  '+IntToStr(day)+'/'+IntToStr(month)+'/'+IntToStr(year)+'   Time :  '+tval);
        writeln(f_tmp,'-----------------------------------------------');
        end;
        closefile(f_dat);
        closefile(f_hrs);
        closefile(f_tmp);
        Edit1.Clear;
        txt:='Pass = '+IntToStr(nizm)+'...��������...';
        Edit1.SelText:=txt;
        f_zero:=0;

        with Moments do //��������
        for x:=num_p+2 to Moments.RowCount do
        begin
              Moments.Cells[1,x]:='';
              Moments.Cells[2,x]:='';
              Moments.Cells[3,x]:='';
              Moments.Repaint;
        end;
//������ �������������
Form1.saveINIFile;
f_scale:='c:/exes/room/scale.ini';//�������
assignfile(f_sh,f_scale);
rewrite(f_sh);
write(f_sh,sh);
closefile(f_sh);

//�����������
f_alr:='c:/exes/room/prehistory.sav';//�����������
assignfile(f_alarm,f_alr);
rewrite(f_alarm);
write(f_alarm,emerg);
closefile(f_alarm);

f_alr:='c:/exes/room/fifteen.shx';//�����������
   assignfile(f_P,f_alr);
   rewrite(f_P);
   write(f_P,buff_P);
closefile(f_P);


            ss[4]:=$53;//S(top)
            WatchF.sendtocomm;//���������� �����������.
end;
end;
end;

procedure TMainsF.Button2Click(Sender: TObject); //�����������
begin
HistF.Showmodal;
end;

procedure TMainsF.Button3Click(Sender: TObject);
begin
Memo4.Clear;
end;

procedure TMainsF.FormCreate(Sender: TObject);
begin
Memo4.Clear;
end;

procedure  TMainsF.ReadText;//������ ��������� ������ �����������
 label m1,m_err_reading;
 var
    ComStat:TComStat;
    i,j: LongInt;
    dwMask,dwError: DWORD;
    OverRead: TOverlapped;
begin
  j:=0;
if f_open=0 then goto m_err_reading;
 BytesRead :=0;
m1:
   ReadFile(commPort, MainsF.ds, SizeOf(MainsF.ds),BytesRead, nil);
   j:=16;
   for i :=1 to 16 do
   begin
    ChChan.Cells[i,1]:=' '+IntToStr(ds[i]-48);
    j:=j-1;
   end;
m_err_reading:
CloseHandle(commPort);
end;



procedure TMainsF.izmer;
 label m1,m2,m3,m4,m_err_reading,again,wt,error_counter;
 const
cal_t:array[1..6] of byte=($54,$54,$54,$54,$24,$24);//T
cal_c:array[1..6] of byte=($43,$43,$43,$43,$24,$24);//T
cal_f:array[1..6] of byte=($46,$46,$46,$46,$24,$24);//F
cal_r:array[1..6] of byte=($52,$52,$52,$52,$24,$24);//R
cal_d:array[1..6] of byte=($44,$44,$44,$44,$24,$24);//D
cal_s:array[1..6] of byte=($53,$53,$53,$53,$24,$24);//S
 var
    BytesWritten: DWORD;
    sDeviceName: array[0..8] of Char;
    c:String;
    rep:char;
    ComStat:TComStat;
dwMask, dwError: DWORD;
OverRead: TOverlapped;
dwRead: DWORD;
Present:TDateTime;
hour,min,sec,msec,i:word;
begin
OpenPort;
m2:
for i:=1 to 40 do m64[i]:=' ';
begin
if ffirstrun=1 then
  begin
   WriteFile(hPort, cal_s,sizeof(cal_s), BytesWritten, nil);//S - T+�+T
   sleep(100);
   WriteFile(hPort, cal_c,sizeof(cal_t), BytesWritten, nil);//T - =LTT�+�L
   sleep(1000);
  end;
   WriteFile(hPort, cal_d, sizeof(cal_d), BytesWritten, nil);//D - +T+=L+ �+�LT-TLTL
end;
i:=0;
m1:
   ReadFile(hPort,m64,sizeof(m64),tread, nil);
   i:=i+1;
   sleep(500);
BytesRead:=tread ;
if i>=20 then goto error_counter;
if BytesRead=0 then  goto m1;
   ffirstrun:=0;
   tint64;
error_counter:
   CloseHandle(hPort);
end;


procedure TMainsF.tint64;
//������� �������� � �3-64
label m1,m2;
var
i:integer;
expn,numb:shortstring;
begin
  begin
   numb:='';
   ichas:=0;
   for i:=1 to 22 do
     begin
        if (m64[i]='e')or(m64[i]='E') then goto m1;
        if (ord(m64[i])>=$30)and(ord(m64[i])<=$39) then
        numb:=numb+(m64[i]);
     end;
m1:
if numb>'' then
try
ichas:=StrToFloat(numb);
except
end;
  try
   expn:=m64[i+2]+m64[i+3];
   if(expn='02')and(length(numb)=4) then ichas:=ichas/10;
   if(expn='01')and(length(numb)=3) then ichas:=ichas/10;
   if(expn='01')and(length(numb)=4) then ichas:=1;
   if ichas>=1e9 then ichas:=ichas/10;
  except
  end;
m2:
end;
end;

procedure TMainsF.Execom;
 label m1,m2,m3,m4,m_err_reading;
 const
cal_t:array[1..6] of byte=($54,$54,$54,$54,$24,$24);//C-��������
cal_c:array[1..6] of byte=($43,$43,$43,$43,$24,$24);//C-��������
cal_o:array[1..6] of byte=($4f,$4f,$4f,$4f,$24,$24);//O-+��������
cal_r:array[1..6] of byte=($52,$52,$52,$52,$24,$24);//R-��������
cal_d:array[1..6] of byte=($44,$44,$44,$44,$24,$24);//D
cal_s:array[1..6] of byte=($53,$53,$53,$53,$24,$24);//S
cal_f:array[1..6] of byte=($46,$46,$46,$46,$24,$24);//F
 var
    BytesWritten: DWORD;
    ComStat:TComStat;
dwError: DWORD;
errpass,l,i:word;
begin
errpass:=0;
DecimalSeparator:='.';
OpenPort;
m2:
m4:
begin
 WriteFile(hPort, cal_o, sizeof(cal_o), BytesWritten, nil);
 sleep(2000);
 WriteFile(hPort, cal_r, sizeof(cal_r), BytesWritten, nil);
 end;
                                //�+�LT-TLT
 begin
 if not ClearCommError(hPort, dwError, @ComStat) then
   raise Exception.Create('Error clearing port');
//   dwRead=ComStat.cbInQue;
   chas:=0;
m1:
   ReadFile(hPort,m64,SizeOf(m64),tread, nil);
   errpass:=errpass+1;
   if errpass>5 then goto m_err_reading;
   if tread=0 then goto m1;
   l:=1;
   while not(m64[l] in['0'..'9']) or (l>39) do l:=l+1;
   str_ichas:='';
   for i:=l to tread-1 do str_ichas:=str_ichas+m64[i];
   contr_str:=str_ichas;
   ichas:=StrToFloat(str_ichas);
   ichas:=ichas*1e9;
   str_ichas:=FloatToStr(ichas);
   if Length(str_ichas)>5 then  Delete(str_ichas,1,Length(str_ichas)-4);
   kont:=StrToFloat(str_ichas);
 end;
m_err_reading:

   errpass:=0;
   CloseHandle(hPort);
   running:=0;
end;

procedure TMainsF.OpenPort;
 var
    DeviceName: array[0..8] of Char;
    c:String;
begin
// �������� �����
      running:=1;
      c:=portmeter;
      StrPCopy(DeviceName,c);
      Dcb.BaudRate:=cbr_57600;
      Dcb.ByteSize:=8;
      Dcb.StopBits:=Onestopbit;
      Dcb.Parity:=Noparity;
      hPort := CreateFile(DeviceName,
     GENERIC_READ or GENERIC_WRITE,
     0,
     nil,
     OPEN_EXISTING,
     FILE_ATTRIBUTE_NORMAL,
     0);
   if hPort = INVALID_HANDLE_VALUE then
           raise Exception.Create('Error opening port')
   else
   if not SetCommState(hPort,Dcb) then
   raise Exception.Create('Error setting port state');
      GetCommTimeOuts(hPort,CommTimeOuts);
   begin
     CommTimeOuts.ReadIntervalTimeout         := 20;
     CommTimeOuts.ReadTotalTimeoutMultiplier  := 0;
     CommTimeOuts.ReadTotalTimeoutConstant    := 250;
     CommTimeOuts.WriteTotalTimeoutMultiplier := 100;
     CommTimeOuts.WriteTotalTimeoutConstant   := 1000;
     SetCommTimeOuts(hPort,CommTimeOuts);
   end;
   if not PurgeComm(hPort, PURGE_TXABORT or PURGE_RXABORT or PURGE_TXCLEAR or PURGE_RXCLEAR) then
   raise Exception.Create('Error purging port ');
   if not SetCommMask(hPort,EV_RXCHAR) then
   raise Exception.Create('Error setting port mask');
//���� ������!
end;
end.




