program PNL;

uses
  Forms,
  panel in 'panel.pas' {Form1},
  coarse in 'coarse.pas' {CoarseF},
  delet in 'delet.pas' {DeleteF},
  Watch in 'Watch.pas' {WatchF},
  pairs in 'pairs.pas' {PairF},
  Main_prog in 'Main_prog.pas' {MainsF},
  prehistory in 'prehistory.pas' {HistF},
  Grafics1 in 'Grafics1.pas' {GrafBox},
  meter in 'meter.pas' {Form2};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title:='PNL';
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TMainsF, MainsF);
  Application.CreateForm(TDeleteF, DeleteF);
  Application.CreateForm(TWatchF, WatchF);
  Application.CreateForm(TPairF, PairF);
  Application.CreateForm(THistF, HistF);
  Application.CreateForm(TCoarseF, CoarseF);
  Application.CreateForm(TGrafBox, GrafBox);
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
