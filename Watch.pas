unit Watch;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Mask, Grids;

type
  TWatchF = class(TForm)
    OKB3: TButton;
    CancelB3: TButton;
    Timer1: TTimer;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    MaskEdit1: TMaskEdit;
    Label2: TLabel;
    WatchMat: TStringGrid;
    Label1: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label3: TLabel;
    procedure OKB3Click(Sender: TObject);
    procedure CancelB3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ResMat_sh;
    procedure WatchMatClick(Sender: TObject);
    procedure Open_c;
    procedure sendtocomm;
    procedure readfromcomm;
    procedure WatchMatDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WatchF: TWatchF;
  numb,expn,txt:string;
  twatch:string;
  last,wts:Real;
DCB: TDCB;
commPort: THandle;
CommTimeOuts:TCommTimeOuts;
  BytesRead :dword;
  hn,mn,sn,h1,h2,h3,m1,m2,m3,s1,s2,s3:word;
  Year, Month, Day:word;
  hour,min,sec,msec:word;
implementation
 uses panel,Main_prog;
{$R *.DFM}

procedure TWatchF.ResMat_sh;
var
x,y,i,j:byte;
begin
WatchMat.Visible:=True;
WatchMat.ColCount:=16;
WatchMat.RowCount:=16;
WatchMat.ScrollBars:=ssNone;

 for i:=0 to 15 do    WatchMat.Cells[0,i+1]:=' '+IntToHex(i,1);
 for j:=0 to 15 do      WatchMat.Cells[j+1,0]:=' '+IntToHex(j,1);

 with WatchMat do
for i:=1 to 16 do
 for j:=1 to 16 do
   cells[i,j]:=IntToHex(j-1,1)+IntToHex(i-1,1);
end;



procedure TWatchF.FormShow(Sender: TObject);
var x,y,i,j:byte;
begin
 WatchMat.Visible:=True;
 ResMat_sh;
 MaskEdit1.SetFocus;
 MaskEdit1.Text:='0';
 OKB3.Enabled:=False;
 CancelB3.Enabled:=False;
end;


procedure TWatchF.OKB3Click(Sender: TObject);
var
i,j:byte;
begin
 txt:='';
 for i:=1 to length(MaskEdit1.Text) do
      if((ord(MaskEdit1.Text[i])>=$30)and(ord(MaskEdit1.Text[i])<=$39))
       then
          txt:=txt+MaskEdit1.Text[i];
 wts:= StrToInt(txt);
 ffirstrun:=1;//���������� �3-64
 Timer1.Enabled:=True;
 Edit1.Visible:=True;
 Edit2.Visible:=True;
 Edit3.Visible:=True;
 Label1.Visible:=True;
 Label6.Visible:=True;
 Label7.Visible:=True;
end;

procedure TWatchF.Timer1Timer(Sender: TObject);
var
i,j,k:byte;
begin
k:=16;
 begin
    if timmeter=0 then MainsF.izmer;
    if timmeter=1 then MainsF.execom;

 txt:=FloatToStrF(ichas,ffFixed,10,1);//����������
 Edit1.Clear;
 Edit1.SelText:=txt+' ��.';
 txt:=FloatToStrF(abs(wts-ichas),ffFixed,10,1);
 Edit2.Clear;
 Edit2.SelText:=txt+' ��.';
 last:=abs(wts-ichas);
 last:=1e9-last;
 txt:=FloatToStrF(last,ffFixed,10,1);
 Edit3.Clear;
 Edit3.SelText:=txt+' ��.';
 end;
end;

procedure TWatchF.CancelB3Click(Sender: TObject);
begin
Timer1.Enabled:=False;
    dw2:=i;//����
    dw1:=j;//�����
    ss[4]:=$53;
    sendtocomm;
close;
end;

procedure TWatchF.WatchMatClick(Sender: TObject);//����� ������� ���.����.
var
i,j,k:byte;
begin
k:=16;
 for i:=0 to 15 do    WatchMat.Cells[0,i+1]:=' '+IntToHex(i,1);
 for j:=0 to 15 do      WatchMat.Cells[j+1,0]:=' '+IntToHex(j,1);

 with WatchMat do
for i:=1 to 16 do
 for j:=1 to 16 do
   cells[i,j]:=IntToHex(j-1,1)+IntToHex(i-1,1);

   with WatchMat do
   begin
   i:=WatchMat.Col;
   j:=WatchMat.Row;

    dw2:=i-1;//����
    dw1:=j-1;//�����


    ss[4]:=$52;

    sendtocomm;
    OKB3.Enabled:=True;
    CancelB3.Enabled:=True;
            p1:=IntToStr(j);
            p2:=IntToStr(i);
            if j>9 then p1:=chr(j+55);
            if i>9 then p2:=chr(i+55);
            txt:='s'+p1+'-s'+p2;
            twatch:=TimeToStr(time);
    assignfile(f_tmp,'c:/exes/'+'logfile.txt');
    if fileexists('c:/exes/'+'logfile.txt')  then append(f_tmp) else  rewrite(f_tmp);
    writeln(f_tmp,'��������� � �������� ���('+txt+') � '+twatch);//LogFile
    closefile(f_tmp);//logfile
   end;
end;



//�������� �����
procedure TWatchF.open_c;
var
   DeviceName: array[0..80] of Char;
   c:String;
   BytesWritten:dword;
 begin
   count:=0;
   c:=portcomm;
   StrPCopy(DeviceName,c);
      Dcb.BaudRate:=cbr_57600;
      Dcb.ByteSize:=8;
      Dcb.StopBits:=Onestopbit;
      Dcb.Parity:=Noparity;
      commPort := CreateFile(DeviceName,
     GENERIC_READ or GENERIC_WRITE,
     0,
     nil,
     OPEN_EXISTING,
     FILE_ATTRIBUTE_NORMAL,
     0);
   if commPort = INVALID_HANDLE_VALUE then
   raise Exception.Create('Error opening port')
   else
   f_open:=1;
   if not SetCommState(commPort,Dcb) then
   raise Exception.Create('Error setting port state');
      GetCommTimeOuts(commPort,CommTimeOuts);
   begin
     CommTimeOuts.ReadIntervalTimeout         := 20;
     CommTimeOuts.ReadTotalTimeoutMultiplier  := 0;
     CommTimeOuts.ReadTotalTimeoutConstant    := 250;
     CommTimeOuts.WriteTotalTimeoutMultiplier := 100;
     CommTimeOuts.WriteTotalTimeoutConstant   := 1000;
     SetCommTimeOuts(commPort,CommTimeOuts);
   end;
   if not PurgeComm(commPort, PURGE_TXABORT or PURGE_RXABORT or PURGE_TXCLEAR or PURGE_RXCLEAR) then
   raise Exception.Create('Error purging port ');
   if not SetCommMask(commPort,EV_RXCHAR) then
   raise Exception.Create('Error setting port mask');
 end;

procedure TWatchF.sendtocomm;//�������� ����� � ����������
 var
   BytesWritten: DWORD;
   i,start,stop,combyte:byte;
 begin
   mass:='';
   open_c;
   stop:=0;
   start:=0;
   combyte:=stop+start;
   ss[1]:=$46;
   ss[2]:=$46;
   ss[3]:=combyte;
   for i:=5 to 7 do ss[i]:=$45;//E
   ss[8]:=$24;
   WriteFile(commPort, ss, sizeof(ss), BytesWritten, nil);
   sleep(100);
   stop:=dw2*16;
   start:=dw1;
   combyte:=stop+start;
   ss[1]:=$46;
   ss[2]:=$46;
   ss[3]:=combyte;
   for i:=5 to 7 do ss[i]:=$45;//E
   ss[8]:=$24;
   WriteFile(commPort, ss, sizeof(ss), BytesWritten, nil);

CloseHandle(commPort);
end;

procedure TWatchF.readfromcomm;//������ ��������� ������ �����������
 var
   BytesWritten: DWORD;
   i,start,stop,combyte:byte;
 begin
   mass:='';
   open_c;
   ss[1]:=$46;
   ss[2]:=$46;
   ss[3]:=$0;
   ss[4]:=$46;//F(ind) ����� � ��������� ������
   for i:=5 to 7 do ss[i]:=$45;
   ss[8]:=$24;
   WriteFile(commPort, ss, sizeof(ss), BytesWritten, nil);
//CloseHandle(commPort);
end;

procedure TWatchF.WatchMatDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
 if(ACol = 0) or (ARow = 0) then exit;
 if(tmp[ACol-1,ARow-1] = 0)  then
 WatchMat.Canvas.Brush.Color := clGreen;
 if(tmp[ACol-1,ARow-1] > 0)  then
 WatchMat.Canvas.Brush.Color := clWhite;
 if((dw2=ACol-1)and(dw1=ARow-1))  then
 WatchMat.Canvas.Brush.Color := clBlue;
 WatchMat.Canvas.FillRect(Rect);
 WatchMat.Canvas.TextOut(Rect.Left,Rect.Top,WatchMat.Cells[ACol,ARow]);
end;

end.


